
let playerElement = document.querySelector(".stream-player");
let audioElement = document.querySelector(".stream-player__audio");
let sourceElement = document.querySelector(".stream-player__source");
let originalSourceUrl = sourceElement.getAttribute("src");
let play_btn = document.querySelector(".stream-player__btn");

let target = 'https://qo2.airtime.pro/api/live-info';

let currentTitle = '';
let currentTimes = ['',''];
let live = false;
let playing = false;


// CHANGE STATES
// -----------------------------------------------------

function set_live(){
    live = true;
    playerElement.classList.add("live");
    playerElement.querySelector(".not-live").style.display = "none";
    playerElement.querySelector(".now-live").style.display = "inline";
    playerElement.querySelector(".current-title").style.display = "inline";
    playerElement.querySelector(".current-times").style.display = "block";
    play_btn.removeAttribute("disabled");
}
function remove_live(){
    live = false;
    playerElement.classList.remove("live");
    playerElement.querySelector(".not-live").style.display = "inline";
    playerElement.querySelector(".now-live").style.display = "none";
    playerElement.querySelector(".current-title").style.display = "none";
    playerElement.querySelector(".current-times").style.display = "none";
    play_btn.setAttribute("disabled", "");
}

function set_playing(){
    playing = true;
    play_btn.querySelector(".ply").style.display = "none";
    play_btn.querySelector(".stp").style.display = "inline";
    playerElement.classList.add("playing");
    audioElement.play();
}
function remove_playing(){
    playing = false;
    audioElement.pause();
    play_btn.querySelector(".ply").style.display = "inline";
    play_btn.querySelector(".stp").style.display = "none";
    playerElement.classList.remove("playing");
}


// AUTO STATES CHECK
// -----------------------------------------------------

function write_times(date){
    return new Intl.DateTimeFormat('en-GB', {timeStyle: 'short'}).format(Date.parse(date));
}

function change_airtime_data(data){

    // console.log("updating?")

    if(data.currentShow.length > 0){

        // console.log("show detected")

        if(!live){
            console.log("setting: live mode")
            set_live();
        }

        let newTitle = data.currentShow[0].name;
        let newTimes = [
            data.currentShow[0].starts, 
            data.currentShow[0].ends
        ];

        // what was playing has changed
        if (newTitle != currentTitle){
            console.log("new show:", newTitle);
            currentTitle = newTitle;
            currentTimes = newTimes;

            let currentTitleElement = playerElement.querySelector(".current-title");
            let startTimeElement = playerElement.querySelector(".start-time");
            let endTimeElement = playerElement.querySelector(".end-time");

            currentTitleElement.innerHTML = currentTitle;
            startTimeElement.innerHTML = write_times(currentTimes[0]);
            endTimeElement.innerHTML = write_times(currentTimes[1]);
        }
    }
    else{
        if(live){
            console.log("setting: offline mode")
            remove_live();
            if(playing){
                remove_playing();
            }
        }
    }
}


// Get API infos:
function request_airtime_data(target, callback){
    // console.log("requesting API");
    let xmlhttp = new XMLHttpRequest();
    let url = target;
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            callback(JSON.parse(this.response));
        }
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}


// every 5 sec we check if data have changed
request_airtime_data(target, change_airtime_data);
setInterval(request_airtime_data, 5000, target, change_airtime_data);


// PLAY BUTTON
// -----------------------------------------------------

// We have to recreate the controls because we want that
// if we pause then play, the stream reset to what is playing now live
// instead of start again from where we had paused (default behavior)
// (or do we ??????) yes because otherwise it will keep downloading

play_btn.addEventListener('click', function(){

    // code kindly sent by Mathieu Serruys
    if (playing === false) {

        if (!sourceElement.getAttribute("src")) {
            sourceElement.setAttribute("src", originalSourceUrl);
            audioElement.load(); // This restarts the stream download
        }
        set_playing();
    } 

    else {
        sourceElement.setAttribute("src", "");
        remove_playing();

        // settimeout, otherwise pause event is not raised normally
        setTimeout(function () {
            audioElement.load(); // This stops the stream from downloading
        });
    }
});

