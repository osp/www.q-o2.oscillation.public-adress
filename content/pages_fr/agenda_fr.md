---
title: Programme
slug: agenda
lang: fr

days:
     
- '26.04':
   day_name: 'Mardi'
   events:
   
   - 'Margherita Brillada - Radiophonix':
      streamed: false
      type: 'workshop'
      time: '14:00 - 17:00'
      location: 'Q-O2'
      practical: |
         Pas de prérequis spécifiques pour cet atelier - anglais parlé.

         Capacité: 15 

         [Inscription requise](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/450744690048)
         
         Matériel requis : écouteurs/casques audios, ordinateur, (interface audio optionnelle) 
         
         Pour toute question, écrivez à [margherita@q-o2.be](mailto:margherita@q-o2.be)

         Envoyez un e-mail à [info@q-o2.be](mailto:info@q-o2.be) pour le tarif de groupe
      description: |
         Au cours de cet atelier, les participants apprendront les bases des logiciels et des technologies de diffusion de radio. Nous explorerons le territoire de l'art radiophonique avec un aperçu de son histoire et de son approche à l'époque contemporaine, donnant un aperçu de la radio en tant qu'espace public/lieu d'art sonore expérimental. Chaque participant devra apporter des matériaux sonores (maximum 5 minutes) pour une séance d'écoute et une discussion ouverte sur leur développement pour une pièce radiophonique.
      image: './images/photo_2022-04-06_18.54.46.jpeg'
      biography: |
         **Margherita Brillada** est une artiste sonore et compositrice de musique électroacoustique basée à Den Haag. En sensibilisant le public et en repensant la radio comme un espace d'exposition pour l'art sonore expérimental, son travail se concentre sur la production d'œuvres d'art radiophoniques caractérisées par des voix et des sonorités instrumentales. Sa pratique implique le field recording et la composition de soundscapes en explorant des systèmes multicanaux afin de créer des expériences d'écoute immersives, interagir avec une audience et donner une voix aux thématiques sociales actuelles.
      links: 
      - 'Website Margherita Brillada': https://margheritabrillada.com/

- '27.04':
   day_name: 'Mercredi'
   events:

   - 'RYBN':
      streamed: false
      type: 'open lab'
      time: '10:00 - 18:00 / sur rendez-vous'
      location: 'Q-O2'
      description: |
         Dans cet open-lab de deux jours, RYBN va planifier, trouble-shoot et discuter des marches des prochains jours avec des invités locaux. Les visites sont bienvenues mais sur rendez-vous en envoyant un email à [info@rybn.org](mailto:info@rybn.org).
         
         Le **Offshore Tour Operator** est un prototype GPS psycho-géographique qui vous guide à travers les 800 000 adresses de la base de données Offshore Leaks de l'ICIJ. Les marches amènent les participants à la recherche des traces physiques de l’offshore banking dans l'architecture de différents quartiers de la ville de Bruxelles. Ainsi, les marches se transforment en une véritable chasse aux sociétés fictives, sociétés fiduciaires, agences de domiciliation, et aux cabinets et agents financiers du shadow banking. A l'issue de chaque marche, une discussion collective offre une plateforme aux participants pour partager leurs expériences et leurs documents, afin de façonner collectivement une image actualisée de la finance qui remet en question la notion même d'offshore.

      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** est un collectif d’artistes extradisciplinaire, créé en 1999 et basé à Paris.
      links: 
      - 'Website RYBN': http://rybn.org/thegreatoffshore/
     
   - 'Lia Mazzari - Whip Cracking':
      streamed: false
      type: 'workshop'
      time: '14:00 - 17:00'
      location: 'Lieu TBA'
      practical: |
         Pas de prérequis spécifiques pour cet atelier- anglais parlé.

         Capacité: 8

         [Inscription requise](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/548020908067)

         Envoyez un e-mail à [info@q-o2.be](mailto:info@q-o2.be) pour le tarif de groupe
      description: |
         Cet atelier enseigne aux participants à balancer et à faire claquer des fouets en tant que dispositif de cartographie sonore pour activer les architectures et les sites qui nous entourent. Nous apprendrons l'histoire des fouets, la terminologie de leurs parties du corps et de leurs craquements, ce qui nous aidera à co-composer une chorégraphie sonore de craquements de fouet, en interaction avec une variété de sons, d'architectures et de matériaux urbains et naturels, en vue de rejouer cette partition le dimanche 1er mai au Festival Oscillation. L'interaction corporelle et la polyrythmie du balancement et du claquement des fouets seront un portail pour écouter nos temps et rythmes peu commun. Le workshop nous permettra également d'enregistrer et d’écrire un catalogue de sons, et de composer un paysage sonore immersif que nous espérons rejouer le dimanche 1er mai dans le cadre du Festival Oscillations. Phénomène viscéral, extension cathartique et souvent trop fétichisée du corps et machine à écouter, le claquement de fouet produit un boum sonique.

         - Portez des vêtements à manches longues
         - Apportez des lunettes/lunettes de protection/lunettes de soleil
         - Apportez une casquette/chapeau
         - Nous devrions proposer des bouchons d'oreilles
         - Apportez vos enregistreurs de son numériques ou microphones (nous les placerons à différents endroits et synchroniserons les enregistrements plus tard)

      image: './images/UGj7yikg.jpeg'
      biography: |
         **Lia Mazzari** attire de nouveaux publics grâce des rencontres avec l'art dans des espaces non conventionnels à travers des performances, des installations et des interventions. Elle crée des manifestations live/enregistrées qui traitent des façons dont le son peut être utilisé comme une force multidimensionnelle de résilience et de mise en commun acoustique. Cette approche pour un activisme sonore emploie régulièrement les enregistrement environnemental, l'instrumentation, la voix et les technologies de transmission.
      links: 
      - 'Website Lia Mazzari': https://liamazzari.com/
   - 'Elena Biserna - Feminist Steps':
      streamed: false
      type: 'workshop'
      time: '19:00 - 22:00'
      location: 'Q-O2'
      practical: |
         Anglais parlé

         Capacité: 10 (pour femme, et personnes queer et de genre non- conforme)

         [Inscription requise](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/405258756791)

         Envoyez un e-mail à [info@q-o2.be](mailto:info@q-o2.be) pour le tarif de groupe
      description: |
         **Workshop nocturne pour femme, et personnes queer et de genre non- conforme**
         
         À partir de quelques partitions textuelles et de protocoles de Pauline Oliveros, du collectif Blank Noise et de moi-même, cet atelier se veut être une plateforme pour réfléchir ensemble sur les expériences (d'écoute) genrées dans l'espace public et pour désapprendre certains des comportements supposés appropriés, sûrs ou prévus lorsque nous marchons. Quelques premiers pas pour questionner les asymétries de rapports de force dans l’espace, et imaginer ensemble des pratiques de care, de solidarité, de réappropriation ou de renversement, susceptibles de nourrir d'autres configurations et pratiques de l’espace.
      image: './images/KsH5q4fM.jpeg'
      biography: |
         **Elena Biserna** est historienne de l’art et commissaire indépendante. Elle écrit, enseigne, accompagne des workshops ou des projets collectifs, fait de la radio et parfois performe. Ses recherches portent sur l’interdisciplinarité en art, l'écoute, les arts sonores, les pratiques artistiques ‘situées’ et leurs relations aux dynamiques urbaines, aux processus socio-culturels, à la sphère publique et politique.
      links: 
      - 'Website Elena Biserna': http://www.q-o2.be/en/artist/elena-biserna/
- '28.04':
   day_name: 'Jeudi'
   events:

   - 'RYBN':
      streamed: false
      type: 'open lab'
      time: '10:00 - 18:00 / sur rendez-vous'
      location: 'Q-O2'
      description: |
         Dans ce open lab de deux jours, RYBN tracera, dépannera et discutera des marches des jours à venir avec des invités locaux. Les visites publiques sont bienvenues sur rendez-vous en envoyant un e-mail à  [info@rybn.org](mailto:info@rybn.org)).
         
         Le **Offshore Tour Operator** est un prototype GPS psycho-géographique qui vous guide à travers les 800 000 adresses de la base de données Offshore Leaks de l'ICIJ. Les marches amènent les participants à la recherche des traces physiques de l’offshore banking dans l'architecture de différents quartiers de la ville de Bruxelles. Ainsi, les marches se transforment en une véritable chasse aux sociétés fictives, sociétés fiduciaires, agences de domiciliation, et aux cabinets et agents financiers du shadow banking. A l'issue de chaque marche, une discussion collective offre une plateforme aux participants pour partager leurs expériences et leurs documents, afin de façonner collectivement une image actualisée de la finance qui remet en question la notion même d'offshore.

      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** est un collectif d’artistes extradisciplinaire, créé en 1999 et basé à Paris.
      links: 
      - 'Website RYBN': http://rybn.org/thegreatoffshore/

   - 'Alisa Oleva - A Listening Walkshop':
      streamed: false
      type: 'workshop'
      time: '11:00 - 13:00'
      location: 'Q-O2'
      practical: |
         Anglais parlé

         Capacité: 12

         [Inscription requise](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/232068394712)

         Envoyez un e-mail à [info@q-o2.be](mailto:info@q-o2.be) pour le tarif de groupe
      description: |
         **Giving your ears to: a listening walkshop**
         
         Nous passerons du temps à écouter, marcher, observer, remarquer, partager. Comment sonne la ville ? Quel son choisiriez-vous de suivre ? Quels sont les sons les plus éloignés que vous pouvez entendre ? La ville s'adresse-t-elle à vous ? Nous travaillerons une partie du temps en groupe tous ensemble, une partie du temps en binôme et vous aurez également du temps pour explorer les choses par vous-même. Tout au long, il y aura des moments pour discuter de nos découvertes.

         **Voir la section "Specials" du site web du festival pour un projet interactif d'Alisa Oleva.**
      image: './images/dxshKMCw.jpeg'
      biography: |
         **Alisa Oleva** traite la ville comme son atelier et la vie urbaine comme matière, considérant les enjeux de la chorégraphie urbaine et de l'archéologie urbaine, les traces et les surfaces, les frontières et les inventaires, les intervalles et les silences, les passages et les fissures. Ses projets ont pris la forme d'une série de situations interactives, de performances, de partitions de mouvements, de rencontres personnelles et intimes, de parkour, de walkshops et de promenades audio.
      links: 
      - 'Website Alisa Oleva': https://www.olevaalisa.com/
  
   - 'doors & food':
      streamed: false
      type: 'start'
      time: '18:00'
      location: 'Decoratelier'
      description: |
         Nourriture par Decoratelier.

   - 'Lia Mazzari':
      streamed: true
      type: 'interjections'
      time: '19:00 - throughout'
      location: 'Decoratelier'
      description: |
         **Intermezzi Sounds: Cracks**

         Un appareil non conventionnel pour la production sonore, transformé en instrument. Phénomène viscéral, extension cathartique, souvent trop fétichisée du corps et de la machine d’écoute, le coup de fouet produit un boum sonique. Lia Mazzari animera un workshop avant le festival, apprenant aux participants à balancer et à faire claquer des fouets en tant que dispositif de cartographie pour activer les architectures et les sites qui nous entourent. Le dimanche 1er mai, les participants exécuteront leur chorégraphie sonore de coups de fouet, dans le cadre du Festival Oscillation.

      image: './images/UGj7yikg.jpeg'
      biography: |
         **Lia Mazzari** attire de nouveaux publics grâce des rencontres avec l'art dans des espaces non conventionnels à travers des performances, des installations et des in-terventions. Elle crée des manifestations live/enregistrées qui traitent des façons dont le son peut être utilisé comme une force multidimension-nelle de résilience et de mise en commun acoustique. Cette approche pour un activisme sonore emploie régulièrement les enregistrement environnemental, l'instrumentation, la voix et les technologies de transmission.
      links: 
      - 'Artist website': https://liamazzari.com/
   - 'Hildegard Westerkamp':
      streamed: True
      type: 'listening session'
      time: '19:00'
      location: 'Decoratelier'
      description: |
        **The Soundscape Speaks - Soundwalking Revisited (2021)**      
        
        L'environnement sonore a beaucoup à nous dire sur la crise climatique à venir. Il "exprime" simplement tout ce qui se passe - et si nous osions vraiment écouter, nous pourrions alors ressentir la profondeur des problèmes environnementaux auxquels le monde est confronté. Au cours de l'année écoulée, j'ai décidé de réexaminer les nombreux enregistrements sonores que j'ai réalisés depuis 1977/78 environ. Sans le savoir alors, il a constitué la base de ma démarche d'enregistrement pour de nombreuses années à venir. L'idée d'inclure ma voix lors de l'enregistrement de l'environnement était présente dès le départ et était basée sur le désir de faire reconnaître ma présence qui enregistre à l'auditeur de la radio. En d'autres termes, en parlant ensemble – environnement acoustique et voix – une relation entre le paysage sonore et l'expression vocale humaine a été révélée.

        **C'est une séance d'écoute; Hildegaard Westerkamp ne sera pas présente physiquement au festival.**
        
        **Voir la section "Specials" du site web du festival pour une autre œuvre de Hildegard Westerkamp.**
      image: './images/Westerkamp_Banff2a.jpeg'
      biography: |
         **Hildegard Westerkamp** est une compositrice, éducatrice et artiste radio canadienne allemande, dont le travail depuis le milieu des années 70 est centré sur le son environnemental et l'écologie acoustique. Ses compositions portent l'attention sur l'acte d'écouter lui-même, sur les espaces intérieurs et cachés des environnements que nous habitons, et sur des détails à la fois familiers et étrangers dans l'environnement acoustique. Elle a écrit de nombreux articles et textes traitant des questions de paysage sonore, d'écologie acoustique et d'écoute, a beaucoup voyagé, donné des conférences et animé des ateliers de soundscape à l'échelle internationale. Elle est membre fondatrice de la WFAE.
      links: 
      - 'Website Hildegard Westerkamp': https://www.hildegardwesterkamp.ca/
   - 'Elena Biserna':
      streamed: true
      type: 'talk'
      time: '19:45'
      location: 'Decoratelier'
      description: |
        Cette conférence porte sur les pratiques et approches féministes de la marche sonore. Partant d'une relecture de la littérature sur la marche et du postulat que l'organisation sociale de l'espace et du temps ne sont pas neutres mais co-produisent des relations hégémoniques, y compris patriarcales, je m’intéresse à des ‘perspectives situées’ (Donna Haraway) fondées sur le genre et la sexualité. Mon objectif est de mettre en dialogue (et de promouvoir) un éventail d'approches qui engendrent d'autres récits de la marche, de la marche sonore et de l'espace public, qui remettent en question l'assignation de certains corps à certains espaces et deviennent ainsi ‘world- making’ - ils envisagent d'autres espaces, ‘des espaces où les choses pourraient se dérouler autrement’ (Leslie Kern).
      image: './images/img_2758.jpg'
      biography: |
         **Elena Biserna** est historienne de l’art et commissaire indépendante. Elle écrit, enseigne, accompagne des workshops ou des projets collectifs, fait de la radio et parfois performe. Ses recherches portent sur l’interdisciplinarité en art, l'écoute, les arts sonores, les pratiques artistiques ‘situées’ et leurs relations aux dynamiques urbaines, aux processus socio-culturels, à la sphère publique et politique.
      links: 
      - 'Website Elena Biserna': http://www.q-o2.be/en/artist/elena-biserna/
   - 'Enrico Malatesta & Attila Faravelli':
      streamed: true
      type: 'live'
      time: '20:15'
      location: 'Decoratelier'
      description: |     
        **Caveja** est le titre d'un grand projet ouvert qui étudie le rôle pro-bable de la technologie ancienne/rurale dans la construction d'une cons-cience auditive contemporaine. Le travail s'approprie des éléments du fol-klore et de la ritualité rurale romagnole afin de produire de multiples productions artistiques qui traitent de musique et d’arts performatifs. Au centre de la recherche se trouvent la Cavéja Dagli Anëll, un ancien dispo-sitif de vibration sonore utilisé en Romagne, équipé d'anneaux métalliques accordés et utilisés à la fois dans les travaux agricoles et dans des ri-tuels superstitieux-propitiatoires spécifics, et le Brontide, un phénomène acoustique inexpliqué, similaire au bruit d'un glissement de terrain ou d'une explosion dans le ciel (skyquakes en anglais),qui n’est plus audible aujourd'hui, et dont la mémoire a été perdue.
       
        Grâce à MET–Museo degli Usi e Costumi della Gente di Romagna di Santarcangelo / Musei Comunali Santarcangelo Xing, Pollinaria A.I.R 1
      image: './images/PV4ww6ko.jpeg'
      biography: |
         **Enrico Malatesta** est un percussionniste et chercheur sonore italien, actif dans le domaine de la musique expérimentale, de l'intervention sonore et de la performance ; sa pratique explore les relations entre le son, l'espace et le corps, la vitalité des matériaux et la morphologie des surfaces, avec une attention particulière aux actes percussifs et aux modes d'écoutes.

         **Attila Faravelli** est un artiste sonore et musicien électro-acoustique italien. Dans sa pra-tique - qui englobe le field recording, les performances, les workshops et le design - il explore l'implication matérielle avec le monde qui nous en-toure. Il fait parti du collectif de recherche sonore Standards à Milan.
      links: 
      - 'Website Enrico Malatesta': https://enricomalatesta.com/
      - 'Website Attila Faravelli': https://auraltools.tumblr.com/
      - 'Youtube': https://www.youtube.com/watch?v=wGfKNnnOWvE&t=1125s
   - 'Kate Carr':
      streamed: true
      type: 'live'
      time: '21:30'
      location: 'Decoratelier'
      description: |
        Cette pièce se concentrera sur la construction d'un paysage sonore à partir d'objets trouvés dans l'espace public, combinés à des field recordings de rue.
      image: './images/yKfiCDKc.jpeg'
      image_credits: '© Dmitri Djuric'
      biography: |
         **Kate Carr** étudie les intersections entre le son, le lieu et l'émotion, en tant qu'ar-tiste et en tant que commissaire, depuis 2010. Pendant ce temps, elle s'est aventurée dans de minuscules villages de pêcheurs du nord de l'Islande, a exploré les rives inondées de la Seine dans une ville centrale électrique, enregistrer la faune en Afrique du Sud et dans les zones humides du sud du Mexique. Son travail se concentre sur l'articulation de nos relations les uns avec les autres, et les espaces dans lesquels nous nous déplaçons à l'aide du son, en mettant l'accent sur la manière dont le son façonne nos expériences du monde ; comment nous le déployons pour nous connecter, nous occuper, nous immerger et nous retirer des lieux, des événements et des autres.      
      links: 
      - 'Website Kate Carr': https://www.gleamingsilverribbon.com/
      
   - 'Lázara Rosell Albear':
      streamed: true
      type: 'live'
      time: '22:15'
      location: 'Decoratelier'
      description: |
        Le projet **Unsurrounded**, a commencé comme un projet solo de performance dans lequel le son, le mouvement, les visuels et les mots se heurtent. Chaque présentation s'appuie sur les archives de matériaux de performances précédentes, de nouvelles entrées et de nouvelles interactions qui peuvent s'étendre à des collaborations avec d'autres interprètes, musiciens, ar-tistes et toutes sortes d'espaces et d'individus (animaux inclus).
      image: './images/0af3tq1k.jpeg'
      biography: |
         **Lázara Rosell Albear** est une artiste cubano-belge avec une pratique cross-media. Dans sa recherche sonore, elle explore les qualités étranges, poly-rythmiques et résonnantes de la batterie, de la trompette de poche et du Shō, combinées avec des sons électroniques et des field recordings arrangés.
      links: 
      - 'Website Lázara Rosell Albear': https://www.mahaworks.org
      
   - 'Davide Tidoni':
      streamed: true
      type: 'live'
      time: '23:00'
      location: 'Decoratelier'
      description: |
         **When Sound Ends** consiste en une série d'actions exécutées avec des mi-crophones et/ou des haut-parleurs. Chaque action se termine par la mort de l'appareil sonore et la disparition conséquente du son. Le projet explore les thèmes de la corporéité, en relation avec la production sonore et l'au-dition, le toucher et la perte, et devient comme une métaphore de la condi-tion humaine et de la nature transitoire de l'existence.
      image: './images/l-o7bRhY.jpeg'
      biography: |
         **Davide Tidoni** est un artiste et chercheur indépendant. Il accorde une at-tention particulière à la dimension physique, perceptive et affective du son, abordant des questions telles que l'interaction avec l'espace acous-tique, l'intersubjectivité, la fugacité et la vulnérabilité des corps. Il s'intéresse également à l'utilisation du son dans la contre-culture et la lutte politique, et a publié une recherche de terrain sur le groupe d’ultras Brescia 1911 (The Sound of Normalisation, 2018).
      links:
      - 'Website Davide Tidoni': http://www.davidetidoni.name/
      - 'Vimeo': https://vimeo.com/583036663

- '29.04':
   day_name: 'Vendredi'
   events:
      
   - 'Attila Faravelli - Aural Tools':
      streamed: false
      type: 'workshop'
      time: '10:00 - 13:00'
      location: 'Q-O2'
      practical: |
        Anglais parlé

         Capacité: 10

         [Inscription requise](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/454423197265)

         Envoyez un e-mail à [info@q-o2.be](mailto:info@q-o2.be) pour le tarif de groupe
      description: |
         L'atelier propose une présentation et une exploration pratique des Aural Tools, qui consistent en une série d'objets conçus pour produire et diffu-ser du son d'une manière que les médias enregistrés traditionnels (LP, CD, numérique) ne peuvent pas. Ce sont des dispositifs assez simples, en bois, en pierre, en carton ou en bâche, qui relient le son et l'espace, l'audi-teur et le corps. Ce ne sont pas des œuvres d'artistes, mais des outils qui documentent le processus de travail des artistes qui les ont réalisées et sont mis à la disposition de tous.
      image: './images/GCeR-6fQ.jpeg'
      biography: |
         **Attila Faravelli** est un artiste sonore et musicien électro-acoustique italien. Dans sa pra-tique - qui englobe le field recording, les performances, les workshops et le design - il explore l'implication matérielle avec le monde qui nous en-toure. Il fait parti du collectif de recherche sonore Standards à Milan.
      links: 
      - 'Artist website': https://auraltools.tumblr.com/
   - 'RYBN':
      streamed: false
      type: 'marche'
      time: '14:00 - 18:00'
      location: 'Lieu TBA'
      practical: |
         Capacité: 20

         [Inscription requise](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/231336394910)

         Envoyez un e-mail à [info@q-o2.be](mailto:info@q-o2.be) pour le tarif de groupe
      description: |
        Le **Offshore Tour Operator** est un prototype GPS psycho-géographique qui vous guide à travers les 800 000 adresses de la base de données Offshore Leaks de l'ICIJ. Les marches amènent les participants à la recherche des traces physiques de l’offshore banking dans l'architecture de différents quartiers de la ville de Bruxelles. Ainsi, les marches se transforment en une véritable chasse aux sociétés fictives, sociétés fiduciaires, agences de domiciliation, et aux cabinets et agents financiers du shadow banking. A l'issue de chaque marche, une discussion collective offre une plateforme aux participants pour partager leurs expériences et leurs documents, afin de façonner collectivement une image actualisée de la finance qui remet en question la notion même d'offshore.
      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** est un collectif d’artistes extradisciplinaire, créé en 1999 et basé à Paris.
      links: 
      - 'Website RYBN': http://rybn.org/thegreatoffshore/
  
   - 'Alisa Oleva':
      streamed: false
      type: 'marche'
      time: '17:00 - 18:00'
      location: 'Atoma'
      practical: |
         Départ: Atoma

         [Inscription gratuite](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/399830470766)
      description: |
         **Listening to where you are not** 
         
         Dans ce workshop, nous commencerons ensemble et rejoindrons un appel audio avec une personne en Ukraine. Ce sera sûrement Olia Fedorova à Kharkiv. Nous nous séparerons ensuite et marcherons dans n'importe quelle direction, écoutant la description de ce qu'Olia entend en même temps à Kharkiv. Quelque part où vous n'êtes pas. Quelque part où le paysage sonore a radicalement changé. Cette expérience nous ramène-t-elle cet endroit ? Ou est-ce que le fait d'écouter les descriptions par quelqu'un du son d'un autre endroit nous y emmène ? Veuillez apporter un casque audio ou des écouteurs et un téléphone chargé et avec une connexion Internet.

         **Voir la section "Specials" du site web du festival pour un projet interactif d'Alisa Oleva.**
      image: './images/AHbrVLYQ.jpeg'
      biography: |
         **Alisa Oleva** traite la ville comme son atelier et la vie urbaine comme matière, considérant les enjeux de la chorégraphie urbaine et de l'archéologie urbaine, les traces et les surfaces, les frontières et les inventaires, les intervalles et les silences, les passages et les fissures. Ses projets ont pris la forme d'une série de situations interactives, de performances, de partitions de mouvements, de rencontres personnelles et intimes, de parkour, de walkshops et de promenades audio.
      links: 
      - 'Website Alisa Oleva': https://www.olevaalisa.com/
  
   - 'doors':
      streamed: false
      type: 'start'
      time: '18:00'
      location: 'Atoma'

   - 'Collective Actions Group':
      streamed: false
      type: 'live'
      time: '19:00'
      location: 'Atoma'
      description: |
        Cette action in-situ pour le Oscillation Festival combine documentation vidéo et dactylographiée réalisée à la station Depot sur la ligne Savelovskaya en janvier 1990, initialement faite pour une installation non réalisée d'Andrei Monastyrsky. Pour Oscillation, cette vidéo, ainsi que d'autres documents dactylographiés de la série **Trips Out of Town**, servent de décor à une chorégraphie-lecture réalisée en collaboration avec des organisateurs du festival. **Interprété par Sabine Hänsgen, Elena Biserna et Henry Andersen.**
      image: './images/f7V9iaVo.jpeg'
      biography: |
         Point central du développement de l'art conceptuel et de la performance russe, le Collective Actions Group a vu le jour en 1976. Le collectif a été fondé par Andrei Monastyrski, Nikita Alexeev, Georgy Kizevalter et Nikolai Panitkov, rejoints ultérieurement par Elena Elagina, Igor Makarevich, Ser-gei Romashko et Sabine Hänsgen. Pendant plusieurs décennies, le groupe a organisé des Trips out of Town, au cours desquels un champ vide dans la campagne moscovite devenait régulièrement le théâtre d'actions minimales impliquant la participation d'un réseau proche d'amis et de collaborateurs.
      links: 
      - 'Website Collective Actions Group': https://conceptualism.letov.ru/KD-ACTIONS.htm
   - 'Bill Dietz ':
      streamed: true
      type: 'lecture performance'
      time: '19:30'
      location: 'Atoma'
      description: |
        **My ears, the police**

        Si l'écoute pouvait jouer un rôle pour repenser l'espace public ; médiations, complicités et mécanismes de normalisation auraient d'abord besoin d'être mieux articulés. À cette fin, les limites juridiques, infrastructurelles et conceptuelles de l'écoute, à la fois celles locales à Bruxelles et les vestiges zombies de 'l'écoute moderne', sont amplifiés pour devenir audibles.
      image: './images/0mNJj6Fk.jpeg'
      biography: |
         **Bill Dietz** est un compositeur, écrivain et coprésident du programme MFA Music/Sound du Bard College. Son travail est souvent présenté dans des festivals, des mu-sées, des immeubles, des magazines et sur la voie publique. Ses publica-tions récentes incluent Maryanne Amacher: Selected Writings and Interviews (co-édité avec Amy Cimini, 2020) et Universal Receptivity (co-écrit avec Kerstin Stakemeier, 2021).
      links: 
      - '#brusselssoundmap': https://www.youtube.com/watch?v=96KzBaBuu7A
      - 'Website Bill Dietz': https://www.tutorialdiversions.org/
   - 'Ryoko Akama & Anne-F Jacques':
      streamed: true
      type: 'live'
      time: '20:00'
      location: 'Atoma'
      description: |
        Filaments chauffant, la lente déformation des matériaux, thermostats comme contrôleurs : dans cette performance, Akama et Jacques explorent la chaleur en tant que force active autour de nous. A travers le son, la lumière, et du mouvement à peine perceptible, la température devient une présence qui se remarque.
      image: './images/BgIEAeDk.jpeg'
      biography: |
         **Ryoko Akama** est une artiste sonore japonaise-coréenne travaillant l'installation, la performance et la composition, résidant à Huddersfield, Royaume-Uni. **Anne-F Jacques** est une artiste sonore basée à Montréal, Canada. Elle s'intéresse à l'amplification, aux interactions obliques entre matériaux et à la cons-truction de divers systèmes idiosyncratiques. Ryoko et Anne-F travaillent ensemble depuis 2016, une collabora-tion basée sur l'échange d'idées absurdes, d'expérimentations ratées ou dangereuses, et d'occasionnelles performances et publications sonores com-munes.

      links: 
      - 'Bandcamp': https://noticerecordings.bandcamp.com/album/evaporation
      - 'Vimeo': https://vimeo.com/247511818
   - 'Matana Roberts':
      streamed: true
      type: 'live'
      time: '21:15'
      location: 'Atoma'
      image: './images/cURayy2w.jpeg'
      biography: |
         **Matana Roberts** est une compositrice mondialement renomée, chef d'orchestre, saxophoniste, expérimentatrice sonore et artiste multimedia. Roberts travaille dans de nombreux contextes et médiums, dont notamment l'improvisation, la danse, la poésie et le théâtre. Elle est peut-être mieux connue pour son projet ac-clamé Coin Coin , un travail en plusieurs chapitres de "panoramic sound quilting" qui vise à exposer les racines mystiques et à canaliser les tra-ditions intuitives de l'expression créative américaine, tout en maintenant un engagement profond et substantiel avec la narrativité, l’histoire, et l’expression communautaire et politique au sein de structures musicales im-provisées.
      links: 
      - 'Website Matana Roberts': https://www.matanaroberts.com/
      - 'Bandcamp': https://matana-roberts.bandcamp.com/album/coin-coin-chapter-four-memphis
   - 'Marta de Pascalis':
      streamed: true
      type: 'live'
      time: '22:00'
      location: 'Atoma'
      description: ''
      image: './images/T4f2UB2A.jpeg'
      image_credits: '© Massimo Pegurri'
      biography: |
         **Marta De Pascalis** est une musicienne et sound designer italienne basée à Berlin. Ses œuvres solo emploient la synthèse analogique et un système de tape-loop, avec le-quel elle crée des motifs de répétition qui créent une sensation de dense, dynamique et cathartique distance. Son son touche un large éventail de genres de musique électronique, dont l’ambient, les premières excursions de l’école de Berlin, le psychédélique et la tape music.
      links: 
      - 'Website Marta De Pascalis': http://www.martadepascalis.com/
      - 'Bandcamp': https://maesia.bandcamp.com/album/sonus-ruinae
   - 'Open Mic with Francesca Hawker':
      streamed: true
      type: 'live'
      time: '23:00'
      location: 'Atoma'
      description: |
         La **Open Mic Night** prendra vraisemblablement la forme d'une promenade col-lective sur Expectation Avenue, guidée d'une représentation à l'autre par les hypothèses de Francesca Hawker, qui, espérons-le, aura eu connaissances préalablement de ce qui se passera. Probablement, l'événement durera envi-ron une heure (hors pauses) et 5 performeurs auront 5 minutes chacun pour faire une proposition bien ajustée et surprenante au public.

         **Pour participer à l'événement**, veuillez envoyer une proposition d'un paragraphe, ainsi que les exigences techniques à **[margherita@q-o2.be](mailto:margherita@q-o2.be)** avant le 22 avril. Les sets sont limités à 5 minutes et doivent être acoustiques, limités à un seul micro ou pouvant être installés directement au mixeur sans sound check ('plug and play'). Les artistes recevront un ticket pour le festival pour le vendredi 29/4 et des bons pour des boissons.

      image: './images/to0hn_oc.jpeg'
      image_credits: '© Nikolaj Jessen'
      biography: |
         **Francesca Hawker** est une artiste britannique qui vit à Bruxelles. Elle joue principalement, en utilisant sa voix, des enregistrements audio et des accessoires rudimen-taires. Elle est titulaire d'une maîtrise du Dutch Art Institute, une aca-démie itinérante basée aux Pays- Bas. Elle est actuellement artiste en ré-sidence à MORPHO à Anvers.
      links: 
      - 'Artist website': https://francescahawker.net/
  
- '30.04':
   day_name: 'Samedi'
   events:
      
   - 'RYBN':
      streamed: false
      type: 'marche'
      time: '11:00 - 15:00'
      location: 'Lieu TBA'
      practical: |
         Capacité: 20

         [Inscription requise](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/736161239426)

         Envoyez un e-mail à [info@q-o2.be](mailto:info@q-o2.be) pour le tarif de groupe
      description: |
        Le **Offshore Tour Operator** est un prototype GPS psycho-géographique qui vous guide à travers les 800 000 adresses de la base de données Offshore Leaks de l'ICIJ. Les marches amènent les participants à la recherche des traces physiques de l’offshore banking dans l'architecture de différents quartiers de la ville de Bruxelles. Ainsi, les marches se transforment en une véritable chasse aux sociétés fictives, sociétés fiduciaires, agences de domiciliation, et aux cabinets et agents financiers du shadow banking. A l'issue de chaque marche, une discussion collective offre une plateforme aux participants pour partager leurs expériences et leurs documents, afin de façonner collectivement une image actualisée de la finance qui remet en question la notion même d'offshore.
      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** est un collectif d’artistes extradisciplinaire, créé en 1999 et basé à Paris.
      links: 
      - 'Website RYBN': http://rybn.org/thegreatoffshore/

   - 'David Helbich':
      streamed: false
      type: 'installation auto-performative'
      time: '14:00 - 16:00'
      location: 'Abbaye de Forest'
      description: | 
         Pas d'inscription requise
         
         **Figures of Walking Together**
         
         La partition de craie sur herbe en trois pistes pour un nombre quelconque de personnes est une chorégraphie sociale, avec des figures et des motifs inspirés des concepts de marche en groupe organisée de manière institutionnelle et intuitive, comme la danse ou l'exercice militaire. Au final, c'est le comportement individuel et collectif des participants auto-performants qui met la lumière sur l'empowerment au sein de ces structures données. Partition à la craie & livret, version Bruxelles 2022.

         **Il s'agit d'un concert en plein air. En cas de pluie, il aura lieu à Zonneklopper. Veuillez garder un œil sur notre site Web pour tout changement.**

      image: './images/DavidHelbich.jpg'
      image_credits: '© David Helbich'
      biography: | 
         **David Helbich** est un artiste sonore, d’installation et de performance, à qui l’on doit diverses œuvres conceptuelles expérimentales pour la scène, les médias, écrits et en ligne, et dans l’espace public. Sa trajectoire oscille entre œuvres, pièces et interventions représentatives et interactives. Bon nombre de ses œuvres traitent d’expériences physiques et sociales concrètes. Un intérêt récurrent est le travail direct avec un public auto-performant
      links: 
      - 'Website David Helbich': http://davidhelbich.blogspot.com/  
      
   - 'Alisa Oleva':
      streamed: false
      type: 'marche'
      time: '15:00 - 16:00'
      location: 'Zonneklopper'
      practical: |
         Départ: Zonneklopper

         Capacité: 10

         [Inscription gratuite](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/971907879517)

      description: |
         **walk - stop - listen - walk again**

         Une marche collective suivant une partition partagée. C'est une invitation à se connecter, à partager et à écouter attentivement. Nous allons bouger en tant que groupe, et à chaque fois que l'un de nous entend quelque chose qu'il veut arrêter et écouter, il le fait, en invitant tout le groupe à s'arrêter et à écouter ensemble. Nous resterons là à écouter jusqu'à ce que quelqu'un d'autre dans le groupe décide de continuer à marcher. Nous répéterons. Nous ne savons pas où nous finirons, où l'écoute nous mènera.

         **Voir la section "Specials" du site web du festival pour un projet interactif d'Alisa Oleva.**
      image: './images/mkSDHMHk.jpeg'
      biography: |
         **Alisa Oleva** traite la ville comme son atelier et la vie urbaine comme matière, considé-rant les enjeux de la chorégraphie urbaine et de l'archéologie urbaine, les traces et les surfaces, les frontières et les inventaires, les intervalles et les silences, les passages et les fissures. Ses projets ont pris la forme d'une série de situations interactives, de performances, de parti-tions de mouvements, de rencontres personnelles et intimes, de parkour, de walkshops et de promenades audio.
      links: 
      - 'Website Alisa Oleva': https://www.olevaalisa.com/

   - 'Pak Yan Lau & Amber Meulenijzer':
      streamed: false
      type: 'live'
      time: '16:00'
      location: 'Abbaye de Forest'
      description: |
        **SAAB SCULPTURES** est une série d’œuvres plaçant la voiture Saab 900 dans différents contextes, ainsi questionnant, transformant, pimpant l’espace public/privé. A travers différents paysages de nouvelles pièces sonores sont réalisées sur mesure à chaque fois. Pour Oscillation Festival, Amber Meulenijzer et Pak Yan Lau collaboreront pour créer une composition et une performance live adaptées à ce système sonore.

         **Il s'agit d'un concert en plein air. En cas de pluie, il aura lieu à Zonneklopper. Veuillez garder un œil sur notre site Web pour tout changement.**
      image: './images/HRrOLVm0.jpeg'
      biography: |
         **Pak Yan Lau**, née en Belgique, avec des racines de Hong Kong et mainte-nant basée à Bruxelles, est une artiste sonore, improvisatrice, musicienne et compositrice, qui a développé au cours des années un riche, dense et captivant univers sonore à base de pianos préparés, de toy pianos, de syn-thés, et de différents objets sonores électroniques. En mêlant avec habili-té différentes approches électro-acoustiques, elle explore le son d’une ma-nière enchanteresse, fusionnant différentes sources sonores avec poésie, magie et finesse.

         **Amber Meulenijzer** a une formation en arts sonores et visuels. Elle explore la tension entre ces domaines, jouant avec la présence et l’absence et le rôle du silence et du spectateur. Où se rencontrent le décor, l’installation et le paysage so-nore ? De quoi le corps a-t-il besoin pour écouter ? SAAB SCULPTURES est le premier projet à transposer ces recherches dans l’espace public.

      links: 
      - 'Website Amber Meulenijzer': https://ambermeulenijzer.tumblr.com/
      - 'Website Pak Yan Lau': https://pakyanlau.com/              
   - 'De zwarte zusters':
      streamed: false
      type: 'live'
      time: '16:45'
      location: 'Abbaye de Forest'
      description: |
         Une improvisation sur le son, l'image et le récit de De zwarte zusters en tant que communauté.

         **Il s'agit d'un concert en plein air. En cas de pluie, il aura lieu à Zonneklopper. Veuillez garder un œil sur notre site Web pour tout changement.**
      image: './images/XOyZZSVE.jpeg'
      image_credits: '© Cato Van Rijckeghem'
      biography: |
         Un groupe de jeunes s'est regroupé autour du nom **De zwarte zusters**. Lors de chaque performance, ils explorent de manière improvisée la tension entre l'aspect musical et l'aspect performatif visuel, dans lequel la recherche des valeurs collectives, l'amitié et la confiance sont centrales. Ils aiment s'inspirer des objets du quotidien, de l'environnement et du jeu libre
      links:
      - 'Artist website': http://zwartezusters.be/
  
   - 'doors & food ':
      streamed: false
      type: 'start'
      time: '17:30'
      location: 'Zonneklopper'
      description: |
         Nourriture par Collective 404.

   - 'Round Table':
      streamed: true
      type: 'talk'
      time: '18:00 - 19:00'
      location: 'Zonneklopper'
      description: |
         **Moving (in) Sound**

         Cette table ronde est l'occasion de discuter ensemble des formats et des dispositifs qui déstabilisent les relations traditionnelles entre l'artiste et le public, à travers le mouvement, les promenades, les itinéraires, les explorations spatiales collectives ou privées. En quoi cela génère-t-il des manières différentes de s'adresser à un public ? Comment cela redéfinit-il la façon dont nous habitons les milieux dont nous faisons partie ? En quoi cela peut-il ouvrir la porte à d'autres pratiques et représentations spatiales ? Comment cela interfère-t-il avec la production (physique, culturelle, sociale, politique) de l'espace public ?

         Avec Alisa Oleva, Bill Dietz, RYBN, David Helbich, modérée par Elena Biserna.
      image: './images/Sa_kbAuE.jpeg'
   - 'Céline Gillain':
      streamed: true
      type: 'talk'
      time: '20:00'
      location: 'Zonneklopper'
      description: |
         **How listening is conditioned by context**

         Céline parlera de la façon dont l'écoute de musique est conditionnée par le contexte, basée sur sa propre expérience en tant que performer. Performer dans un festival de musique, une institution artistique, une salle de concert ou un club sont des expériences très différentes, avec des conditions, des règles et des attentes différentes. Elle s’est intéressée à questionner le clivage que cela crée entre les publics et comment il est enraciné dans des questions liées à la classe, au sexe et à la race.
      image: './images/CVtiFED8.jpeg'
      image_credits: '© Claudia Höhne'
      biography: |
         **Céline Gillain** est une artiste dont le travail explore les points de rencontre entre différents domaine de l’art et de la musique, tels que la performance, le storytelling, la musique expérimentale et la musique électronique, en déconstruisant les codes et les hiérarchies qui les gouvernent. Dévoilant la précarité toujours croissante de l'artiste, son travail interroge les mécanismes par lesquels les idées sont transformées en marchandises dans la culture mainstream.
      links: 
      - 'Website Céline Gillain': https://linktr.ee/CelineGillain  
   - 'Peter Kutin with Stan Maris':
      streamed: true
      type: 'live'
      time: '20:30'
      location: 'Zonneklopper'
      description: |
         Peter Kutin donnera un concert improvisé avec l'accordéoniste Stan Maris. Le son de ce dernier sera mis en œuvre et contrebalancé par les Light-to-Noise Feedback Systems de Kutin - son et lumière projetteront des ombres.
      image: './images/Gh7mDYh0.jpeg'
      biography: |
         **Peter Kutin** travaille avec le son à travers les genres, les supports et les médias. Poussé par une curiosité bouillonnante soutenue par un esprit de collaboration, Kutin a écrit et développé des environnements musicaux et sonores pour le cinéma, le théâtre, la performance, la danse contemporaine et la radio, ainsi que la réalisé plusieurs courts métrages expérimentaux lui-même.
         
         **Stan Maris** est un accordéoniste et compositeur belge, princi-palement actif dans la scène musicale d’improvisation. Il joue et compose pour ses propres projets Kreis, Ocean Eddie, Erem et ses performances en solo. En tant que sideman, Stan est actif dans des groupes comme Suura, Ma-thieu Robert Group et le New Movement Ensemble de Giovanni Di Domenico.

      links: 
      - 'Website Peter Kutin': http://kutin.klingt.org/
      - 'Website Stan Maris': https://www.stanmaris.com/

   - 'Mariam Rezaei':
      streamed: true
      type: 'live'
      time: '21:30'
      location: 'Zonneklopper'
      description: |
         **BOWN** est turntablism expérimental de la compositrice et DJ Mariam Rezaei. À l'aide de deux platines, Mariam improvise avec des échantillons vocaux et instru-mentaux dans une improvisation qui relie à la fois ses origines iranienne et anglaise. Les timbres oscille entre réductionnisme, noise, free jazz et absurdisme avec une pointe d'opéra et de hiphop.
      image: './images/ARTKJWWE.jpeg'
      biography: |
         **Mariam Rezaei** est une compositeur, turntabliste et interprète primée. Elle dirige le pro-jet d’art expérimental TOPH à Gateshead. TOPH organise régulièrement des séries concerts, ansi que annuellement TUSK FRINGE et TUSK NORTH pour le TUSK Festival. Ses dernières sorties inclues ‘SKEEN’ chez Fractal Meat Cuts, ‘The End of The World...Finally’ avec Sam Goff chez Cacophonous Revi-val Recordings, ‘il’, une collaboration avec Stephen Bishop chez TUSK Edi-titions, et ‘SISTER’ avec la soprano Alya Al-Sultani, chez Takuroku.
      links: 
      - 'Artist website': https://mariam-rezaei.com/
   - 'Thomas Ankersmit':
      streamed: true
      type: 'live'
      time: '22:15'
      location: 'Zonneklopper'
      description: |
         Thomas Ankersmit utilise des systèmes de sonorisation et son synthétiseur analogique Serge Modular pour activer des espaces et des corps physiques réels par le son, ou plutôt pour suggérer des espaces imaginaires, souvent en contraste les uns avec les autres. Inspiré par le travail de par ex. Maryanne Amacher et Dick Raaijmakers, il explore le potentiel des fréquences de résonance et des émissions oto-acoustiques pour "pirater" des systèmes de sonorisation stéréo simples pour une manière d'écoute plus physique et tridimensionnelle. S'il utilise exclusivement des sons analogiques-électroniques, le résultat a néanmoins souvent un caractère organique et landscape-like.

      image: './images/P1VW05GQ.jpeg'
      image_credits: '© Mich Leemans'
      biography: |
         **Thomas Ankersmit** est un musicien et un sound artist basé à Berlin et à Amsterdam. Il joue avec le synthétiseur Serge Modular, live et en studio. Sa musique est publié sur les labels Shelter Press, PAN, et Touch, et combine de complexe détail sonore et la brut puissance électrique, avec une expérience très physique et spatiale du son.
      links: 
      - 'Website Thomas Ankersmit': https://thomasankersmit.net/
      - 'Soundcloud': https://soundcloud.com/weerzin/perceptual-geography-excerpt-live-at-ctm-festival-2019
   - 'DJ Marcelle':
      streamed: true
      type: 'live'
      time: '23:00'
      location: 'Zonneklopper'
      image: './images/rhkAjYyk.jpeg'
      biography: |
         'Bien que **DJ Marcelle** collectionne de la musique depuis plus longtemps que la plupart d'entre nous, elle se sent toujours comme la sweetheart des Pays-Bas. C'est une artiste avec une approche espiègle, presque ironique et qui enfreint les règles de son DJing, de sa production et de son animation radio, taillés par sa présence d’esprit caractéristique. Reconnue pour sa configuration à trois platines, DJ Marcelle crée des compositions à partir de chansons et des symphonies à partir de ses mix – heurtant des genres disparates, des extraits vocaux appropriés, et des soundscapes déformés dans un melting-pot à la Frankenstein. Ses performances sont inventives, euphoriques et surtout puissantes.' —Resident Advisor
      links:
      - 'Website DJ Marcelle': https://filhounico.com/booking/84

- '01.05':
   day_name: 'Dimanche'
   events:
      
   - 'RYBN':
      streamed: false
      type: 'marche'
      time: '11:00 - 15:00'
      location: 'Lieu TBA'
      practical: |
         Capacité: 20

         [Inscription requise](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/644099568474)

         Envoyez un e-mail à [info@q-o2.be](mailto:info@q-o2.be) pour le tarif de groupe
      description: |
        Le **Offshore Tour Operator** est un prototype GPS psycho-géographique qui vous guide à travers les 800 000 adresses de la base de données Offshore Leaks de l'ICIJ. Les marches amènent les participants à la recherche des traces physiques de l’offshore banking dans l'architecture de différents quartiers de la ville de Bruxelles. Ainsi, les marches se transforment en une véritable chasse aux sociétés fictives, sociétés fiduciaires, agences de domiciliation, et aux cabinets et agents financiers du shadow banking. A l'issue de chaque marche, une discussion collective offre une plateforme aux participants pour partager leurs expériences et leurs documents, afin de façonner collectivement une image actualisée de la finance qui remet en question la notion même d'offshore.
      image: './images/PHaLcF-Q.jpeg'
      biography: |
         **RYBN** est un collectif d’artistes extradisciplinaire, créé en 1999 et basé à Paris.
      links: 
      - 'Website RYBN': http://rybn.org/thegreatoffshore/
      links: 
      - 'Collective website': http://rybn.org/thegreatoffshore/
   - 'Jérôme Giller':
      streamed: false
      type: 'marche'
      time: '13:30 - 15:30'
      location: 'Union tram stop'
      practical: |
         français parlé
         
         Capacité: 20

         [Inscription requise](https://apps.ticketmatic.com/shop/qo2/shop/addtickets/352094840913)

         Envoyez un e-mail à [info@q-o2.be](mailto:info@q-o2.be) pour le tarif de groupe
      description: |
        **Forest-Village : archipel d’habitats**
        
        Dans cette marche, Jérôme Giller propose d’arpenter le bas de Fo-rest en reliant des îlots d’habitations qui permettent de comprendre l’évolution historique, sociologique et économique du foncier du bassin in-dustriel, distribué autour de la ligne de chemin de fer 124 qui relie Bruxelles à Charleroi.
        
        Départ de la marche:
        
        Arrêt de tramway Union (ligne 82 et 97): Avenue Van Volxem 208 / 1190 Forest

      image: './images/CC7gWQg0.jpeg'
      image_credits: '© Archives Jérôme Giller'
      biography: |
         **Jérôme Giller** lives and works in Brussels where he leads a reflection on urban and peri-urban territorialities using walking as a method and tool of artistic creation. He surveys the territories following lines of geographical, urban, historical, and poetic wanderings. Giller's interventions are furtive and immaterial. These are moments to live and experience; laboratories that infiltrate reality.
      links: 
      - 'Artist website': http://www.jeromegiller.net/
   - 'David Helbich':
      streamed: false
      type: 'installation auto-performative'
      time: '14:00 - 16:00'
      location: 'Abbaye de Forest'
      description: | 
         Pas d'inscription requise

         **Figures of Walking Together**
         
         La partition de craie sur herbe en trois pistes pour un nombre quelconque de personnes est une chorégraphie sociale, avec des figures et des motifs inspirés des concepts de marche en groupe organisée de manière institutionnelle et intuitive, comme la danse ou l'exercice militaire. Au final, c'est le comportement individuel et collectif des participants auto-performants qui met la lumière sur l'empowerment au sein de ces structures données. Partition à la craie & livret, version Bruxelles 2022.

         **Il s'agit d'un concert en plein air. En cas de pluie, il aura lieu à Zonneklopper. Veuillez garder un œil sur notre site Web pour tout changement.**
      image: './images/DavidHelbich.jpg'
      image_credits: '© David Helbich'
      biography: | 
         **David Helbich** est un artiste sonore, d’installation et de performance, à qui l’on doit diverses œuvres conceptuelles expérimentales pour la scène, les médias, écrits et en ligne, et dans l’espace public. Sa trajectoire oscille entre œuvres, pièces et interventions représentatives et interactives. Bon nombre de ses œuvres traitent d’expériences physiques et sociales concrètes. Un intérêt récurrent est le travail direct avec un public auto-performant
      links: 
      - 'Website David Helbich': http://davidhelbich.blogspot.com/  
      
      
   - 'Lia Mazzari':
      streamed: false
      type: 'live'
      time: '16:00'
      location: 'Abbaye de Forest'
      description: |
         **Whipping Music**

         Un appareil non conventionnel pour la production sonore, transformé en instrument. Phénomène viscéral, extension cathartique, souvent trop fétichisée du corps et de la machine d’écoute, le coup de fouet produit un boum sonique. Lia Mazzari animera un workshop avant le festival, apprenant aux participants à balancer et à faire claquer des fouets en tant que dispositif de cartographie pour activer les architectures et les sites qui nous entourent. Le dimanche 1er mai, les participants exécuteront leur chorégraphie sonore de coups de fouet, dans le cadre du Festival Oscillation.

         **Il s'agit d'un concert en plein air. En cas de pluie, il aura lieu à Zonneklopper. Veuillez garder un œil sur notre site Web pour tout changement.**
      image: './images/UGj7yikg.jpeg'
      biography: |
         **Lia Mazzari** attire de nouveaux publics grâce des rencontres avec l'art dans des espaces non conventionnels à travers des performances, des installations et des in-terventions. Elle crée des manifestations live/enregistrées qui traitent des façons dont le son peut être utilisé comme une force multidimension-nelle de résilience et de mise en commun acoustique. Cette approche pour un activisme sonore emploie régulièrement les enregistrement environnemental, l'instrumentation, la voix et les technologies de transmission.
      links: 
      - 'Artist website': https://liamazzari.com/
   - 'BMB con.':
      streamed: false
      type: 'live'
      time: '16:30'
      location: 'Abbaye de Forest'
      description: |
        BMB con. est en train développer une nouvelle performance in-situ pour le festival Oscillation.

         **Il s'agit d'un concert en plein air. En cas de pluie, il aura lieu à Zonneklopper. Veuillez garder un œil sur notre site Web pour tout changement.**
      image: './images/zB0NNcvc.jpeg'
      biography: |
         Au cours des 30 dernières années, **BMB** con. ont joué l’équilibriste entre la musique et le bruit, le son et l'image, l'intérieur et l'extérieur, le nu-mérique et l'analogique, l'interprète et le public. Fondé par Roelf Toxo-peus, Justin Bennett et Wikke 't Hooft en 1989. Depuis 2006 BMB con. se compose d'un duo travaillant avec un groupe changeant d'artistes / inter-prètes invités. Ils intègrent la musique électronique et acoustique, le ci-néma, la vidéo et le théâtre physique dans leurs performances et installa-tions. En plus de ces actions éphémères et souvent uniques, BMB con. crée et publie des CD, des cassettes, des vidéos et des photographies.
      links: 
      - 'Artist website': http://www.bmbcon.org/
  
   - 'doors & food':
      streamed: false
      type: 'start'
      time: '17:00'
      location: 'Zonneklopper'
      description: |
         Nourriture par Collective 404.

   - 'Label Market':
      streamed: false
      type: 'live'
      time: '17:00 - 21:00'
      location: 'Zonneklopper'
      description: |
         Un marché aux disques et aux livres de nos labels indépendants locaux favoris et de la librairie nomade ¨**Underbelly**.
      biography: |
   - 'RYBN':
      streamed: true
      type: 'listening session'
      time: '17:30'
      location: 'Zonneklopper'
      description: |

        RYBN résumera leurs expériences de la semaine passée, à réaliser le Offshore Tour Operator à Bruxelles.

         Le **Offshore Tour Operator** est un prototype GPS psycho-géographique qui vous guide à travers les 800 000 adresses de la base de données Offshore Leaks de l'ICIJ. Les marches amènent les participants à la recherche des traces physiques de l’offshore banking dans l'architecture de différents quartiers de la ville de Bruxelles. Ainsi, les marches se transforment en une véritable chasse aux sociétés fictives, sociétés fiduciaires, agences de domiciliation, et aux cabinets et agents financiers du shadow banking. A l'issue de chaque marche, une discussion collective offre une plateforme aux participants pour partager leurs expériences et leurs documents, afin de façonner collectivement une image actualisée de la finance qui remet en question la notion même d'offshore.

      image: './images/PHaLcF-Q.jpeg'
      biography: |
         RYBN.ORG est un collectif d’artistes extradisciplinaire, créé en 1999 et basé à Paris.
      links: 
      - 'Website RYBN': http://rybn.org/thegreatoffshore/
   - 'Leandro Pisano':
      streamed: true
      type: 'talk'
      time: '18:00'
      location: 'Zonneklopper'
      description: |
       **The Manifesto of Rural Futurism** est une invitation à vivre les espaces ruraux comme des espaces pour questionner notre approche à l’histoire et au paysage. Dans ce texte, écrit en 2019 par Leandro Pisano et Beatrice Ferra-ra, les pratiques d'écoute sont déployées comme un moyen de traverser de manière critique les ‘territoires frontaliers’ des territoires ruraux, re-mettant en question les notions persistantes de ‘marginalité inéluctable’, de ‘résidualité’ et de ‘périphéralité’.
      image: './images/oh-uMGU0.jpeg'
      biography: |
         **Leandro Pisano** est un curateur, écrivain et chercheur indépendant qui s'intéresse aux intersections entre l'art, le son et la technoculture. Le domaine spécifique de ses recherches porte sur l'écologie politique des territoires ruraux et marginaux. Il est le fondateur du festival Inter-ferenze/Liminaria et a organisé des expositions d'arts sonores en Italie, au Chili et en Australie.
      links: 
      - 'Youtube': https://www.youtube.com/watch?v=7WqDmUp9SHU&feature=youtu.be&ab_channel=Liminaria
      - 'Website Leandro Pisano': https://www.leandropisano.it/en/
   - 'Fausto Cáceres (Shirley & Spinoza)':
      streamed: true
      type: 'live'
      time: '18:30'
      location: 'Zonneklopper'
      description: |
         **Street Cries & the Wandering Song** est un collage lyrique de la texture et de la vie qui traversait la petite intersection pavée sous la fenêtre de mon studio à Dali, en Chine. Les cris mélodiques des colporteurs, des recycleurs et des scènes spontanées ont été capturés essentiellement entre 2006 et 2020, avec un microphone stéréo sus-pendu au-dessus de la rue, ou avec des microphones binauraux placés à d'autres endroits.
      image: './images/ROGC_sandangel.jpg'
      biography: |
         **Fausto Cáceres** est un créateur/collectionneur de sons américain, récemment relocalisé en Nouvelle-Zélande après avoir vécu 15 ans dans les périphéries de la Chine continentale, où il a largement documenté la musique traditionnelle des cultures des minorités, ainsi que le paysage sonore en constante évolution de la RPC. Il est également le ‘Remote Operator’ de la long-running Shirley & Spinoza Radio.
      links: 
      - 'Artist website': https://beacons.ai/shirleyandspinoza      
   - 'Jasmine Guffond':
      streamed: true
      type: 'live'
      time: '19:00'
      location: 'Zonneklopper'
      description: |
         **Listening to Listening to Listening** est une performance installative dans laquelle Jasmine Guffond exécute des coupures, des boucles aléatoires et des manipulations de hauteur d'enregistrements de terrain réalisés par Margherita Brillada dans et autour du bâtiment Zonneklopper. Transmis via des transducteurs fixés à des points spécifiques de la Salle Mouvement de Zonneklopper, le potentiel du son en tant que force vibratoire est activé pour faire résonner la matérialité de la pièce elle-même. Le son à la fois singulier et universel affecte les corps et la matière de manière unique tout en les enveloppant à la fois d'intensités différentes. Qu'est-ce que cela pourrait signifier d'écouter collectivement mais de manière non équivalente alors que le public est invité à écouter non seulement le bâtiment mais Jasmine écoutant Margherita. L'écoute de l'écoute pourrait-elle être une technique d'appréciation de la différence.
      image: './images/jasmine-guffond.jpg'
      biography: |
         **Jasmine Guffond** est une artiste et compositrice travaillant à l'interface des infrastructures sociales, politiques et techniques. Axée sur la composition électronique dans des contextes musicaux et artistiques, sa pratique couvre la performance live, l'enregistrement, l'installation et les add-ons pour navigateur fait sur mesure. À travers la sonification des données, elle aborde le potentiel du son à s'engager dans des questions politiques contemporaines et engage l'écoute en tant que pratique de connaissance située. Elle a exposé et joué live à l'international et a sorti des disques en solo avec les labels Sonic Pieces (2015, 2017), Karl Records (2018) et Editions Mego (2020).
      links: 
      - 'Artist website': http://jasmineguffond.com/
         
   - 'Aymeric de Tapol':
      streamed: true
      type: 'live'
      time: '20:00'
      location: 'Zonneklopper'
      description: |
         Aymeric de Tapol écrit : (a lire à voix haute dans sa tête) 'Cette musique est basée sur l’écoute d’un cluster de signaux composé de séquences écrites pour synthé-tiseur analogique . C’est la première fois que la musique me semble être elle-même le phénomène, c'est-à- dire qu’elle est un peu plus au centre de son histoire et est toujours questionable. En somme c' est l’observation d' une répétition de sons en mouvement : la polyrythmie.'
      image: './images/BpzCNIdA.jpeg'
      biography: |
         **Aymeric de Tapol** pratique une musique nommée expérimentale ainsi que la prise de son de ter-rain. Il collabore avec d’autres artistes autour du cinéma, du documen-taire, de la radio indépendante et de la performance. Il est membre du duo "Cancellled" avec Yann Leguay ainsi que du collectif "p-node". Ces musiques sont éditées par des labels indépendants tels que Tanuki, Angström, Vlek, Lexi disque, Tanzprocesz et Knotwilg records.
      links: 
      - 'Bandcamp': https://aymeridetapol.bandcamp.com/track/concert-biennale-du-mans-2022
---
