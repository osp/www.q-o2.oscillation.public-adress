---
title: Stream
default_state: 'disabled'
column: 1
order: 2
overflow: none
---

<div class="stream-player">
    <button class="stream-player__btn" disabled>
        <span class="ply">
            <svg viewBox="0 0 50 50" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <path d="M 0,0 L 50,25 L 0,50 Z" />
            </svg>
        </span>
        <span class="stp" style="display:none;">
            <svg viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg">
                <rect width="50" height="50" x="0" y="0" />
            </svg>
        </span>
    </button>
    <div class="stream-player__now">
        <span class="current-status">
            <span class="not-live">Stream starts Saturday 30, 18:00 Brussels time</span>
            <span class="now-live" style="display:none;">Now live:</span>
        </span>
        <span class="current-title" style="display:none;"></span>
        <div class="current-times" style="display:none;">
            <span class="start-time"></span> 
            <span class="time-separator">&nbsp;-&nbsp;</span> 
            <span class="end-time"></span>
        </div>
    </div>
    <audio class="stream-player__audio">
        <source class="stream-player__source" src="http://qo2.out.airtime.pro:8000/qo2_b" type="audio/mpeg">
    </audio>
</div>
