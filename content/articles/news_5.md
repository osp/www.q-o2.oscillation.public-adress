---
title: The Oscillation Bulletin Episode 5
date: 04-28
slug: news-5
image:
section: blog
---

Dear oscillators,

Tonight marks the grand opening of the Oscillation ::: Public Address Festival at the wonderful Decoratelier in Molenbeek. Doors open at 18:00, and we take away your logistics sorrows about when and where to eat tonight, since food will also be available. The first act starts at 19:00, and there are still tickets left at the door.

The first act, that is a listening sessions of **The Soundscape Speaks - Soundwalking Revisited (2021)** by Hildegard Westerkamp, a German Canadian composer, educator and radio artist whose work since the mid-seventies has centre around environmental sound and acoustic ecology. Note: this is a listening session; Hildegaard Westerkamp will not be physically present at the festival.

The listening session is followed by the talk **Sound Walking As A Feminist** by independent art historian and curator Elena Biserna. The talk focuses on feminist practices and approaches in soundwalking. And she aims to put in dialogue (and to promote) an array of approaches that engender other narratives of walking, soundwalking and public space, that challenge the assignation of certain bodies to certain spaces and thus become 'world-making' – they envision other spaces, 'spaces where things could unfold otherwise' (Leslie Kern).

At 20:15, Enrico Malatesta & Attila Faravelli will perform **Caveja**, an open project investigating the possible role of ancient/rural technology in the construction of a contemporary aural awareness. The work appropriates elements of folklore and rural Romagna rituality in order to produce multiple artistic outputs that deal with music and performative arts.

Next up is Kate Carr, who focus on building a soundscape from objects found in public space, combined with field recordings of the streetscape. She has been investigating the intersections between sound, place, and emotionality; her work centres on articulating our relationships with each other and the spaces we move through using sound.

We end the night with Lázara Rosell Albear, who will perform **Unsurrounded**, a project that started as a solo performance in which sound, movement, visuals and words collide. Each presentation draws on new inputs and new interactions that can extent to collaborations with other performers, musicians, artists and all kind of spaces and individuals (animals included).

Forever's gonna start tonight,

The Oscillation Crew
