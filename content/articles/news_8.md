---
title: The Oscillation Bulletin Episode 8
date: 05-1
slug: news-8
image: './images/unnamed.png'
image_credits: 'De zwarte zusters yesterday at Place Saint-Denis.'
section: blog
---

Dearest oscillators,

Today already marks the grand finale of our annual celebration. Still high-spirited after yesterday's performances, talks, walks and wonky dance moves, we're down at the amazing Zonneklopper and Abbaye de Forest sites for another day, with performances, talks and a label market. Tickets are still available at the door, and like the past few days, all acts are live relayed on our website.


You have another chance today to experience David Helbich's Figures of Walking at the Abbaye De Forest today. His score of chalk on grass in up to three tracks for any number of people is a social choreography, with figures and patterns inspired by concepts of institutionally and intuitively organized walking in groups, such as in dance or military drill. The individual and collective behavior of the self-performing participants sheds light on empowerment within such given structures. It opens at 14:00, and we can highly recommend you to perform it yourself, after doing that ourselves multiple times yesterday.

Lia Mazzari will perform Whipping Music at 16:00, on the same site. She hosted a workshop prior to the festival, teaching participants to swing and crack whips as an investigatory sonic mapping device to activate the architectures and sites around us. For this performance, the participants will perform their sonic choregraphy of whip cracks.

They are followed by BMB con., who developed a new site-specific performance for the Oscillation festival. For the last 30-odd years, they have been performing a balancing act between music and noise, sound and vision, indoors and out, digital and analogue, performer and audience. 

At 17:00, we're opening the doors of Zonneklopper again, where food will be available. Perfectly fitting for a Sunday, we're hosting a record fair and bookshop by our favourite local independent labels (KRAAK, Ediçoes CN, Futura Resistenza, Lexi Disques, ...) and the nomadic bookshop Underbelly.

Half an our later, one of this week's central collectives, RYBN, will be summarising their experiences of the past week, realising the Offshore Tour Operator in Brussels, a psycho-geographic GPS prototype that guides you through the 800,000 addresses of the ICIJ's Offshore Leaks database. They hosted walks that bring the participants to search for the physical traces of offshore banking within the architecture of various neighborhoods of the city of Brussels.

At 18:00, Leandro Pisano hosts a talk about The Manifesto of Rural Futurism, an invitation to experience rural locations as spaces in which to question our approach to history and landscape. In this text, written in 2019 by Leandro Pisano and Beatrice Ferrara, listening practices are deployed as ways to critically traverse the 'border territories' of rural space, challenging persisting notions about 'inescapable marginality', 'residuality' and 'peripherality'.

He is followed by Fausto Cáceres (Shirley & Spinoza), with a remote performance live from New Zealand. He performs Street Cries & the Wandering Songs, a lyrical collage of the texture and life that traversed the little cobblestone intersection beneath his studio window in Dali, China. The melodic cries of peddlers, recyclers and spontaneous scenes were captured between 2006 and 2020, primarily with a stereo microphone suspended above the street or with binaural mics in other locations.

At 19:00, Jasmine Guffond takes the stage. She is an artist and composer working at the interface of social, political and technical infrastructures. Focused on electronic composition across music and art contexts her practice spans live performance, recording, installation and custom made browser add-on. Through the sonification of data she addresses the potential of sound to engage with contemporary political questions and engages listening as a situated knowledge practice.

She will perform Listening to Listening to Listening, an installative performance whereby she performs cut ups, random loopings and pitch manipulations of field recordings made by Margherita Brillada in and around the Zonneklopper building. Transmitted via transducers attached to specific points within Zonneklopper’s Salle Mouvement, sound’s potential as a vibrational force is activated to resonate the materiality of the room itself. What could it mean to listen collectively yet non-equivalently as the audience is invited to listen not only to the building but to Jasmine listening to Margherita. Could listening to listening be a technique for the appreciation of difference?

The festival's closer is Aymeric de Tapol who wrote this about his performance (read aloud in your head): 'This music is based on the listening of a cluster composed by written sequences for analogue synthesizer. It is the first time that the music seems to me to be the phenomenon itself, meaning that it is more at the centre of its history and is always questionable. In short, it is the observation of a repetition of moving sounds: the polyrhythm.'

We don't like mondays,

The Oscillation Crew

