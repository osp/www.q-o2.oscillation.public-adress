---
title: Hildegard Westerkamp
section: specials
---


**Exploring Balance & Focus in Acoustic Ecology**

A network of listening paths presented by Hildegard Westerkamp at the International Conference of the World Forum of Acoustic Ecology in Corfu, Greece in 2011. **[Listen here](https://soundcloud.com/q-o2/hildegaard-westerkamp-exploring-balance-focus-in-acoustic-ecology/s-jyDxikS2Ioz?utm_source=clipboard&utm_medium=text&utm_campaign=social_sharing)**

Acoustic Ecology is the research field created by the World Soundscape Project, an international research project founded by Murray Schafer, at the Simon Fraser University in Vancouver (CA) in the 1960s. As one of the core members of the project, Westerkamp developed her research about attention and listening awareness within soundscapes, and her educational approach through sound walks, field recording and radiophonic works, as well as her long-standing compositional practice. 

In this talk she guides through the following questions: 'How can we find focus and balance in such a vast interdisciplinary intercultural field? In a field that has listening as its very core of study, and educational action, and therefore naturally touches on all aspects of life?'
